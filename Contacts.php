<?php

class Contacts {

    // This will be stored in a database instead of this, 
    // I am storing it in this variable (like a cached variable), to avoid the deployment because of database dependency.
    private $contactsList = array();

    public function run() {
        do {
	    //Menu 
	    $input = -1;
	    $this->showMenu($input);
            $isValid = $this->validateInput($input);
            if($isValid) {
		$this->contactsOptions($input);
            }
        } while($input != 3);
    }

    private function contactsOptions($option) {
	switch($option) {
	    case 1:
 	        $this->addContacts();
		break;
	    case 2:
		$this->searchContacts();
		break;
	    default:
		return false;
        }
    }

    private function addContacts() {
	echo "Please enter your name" . "\n";
	$fp = fopen("php://stdin", "r");
	$name = ltrim(rtrim(fgets($fp, 100)));
	$nameArray = $this->validateInputValue($name);
	$firstName = $nameArray[0];
	$lastName = isset($nameArray[1]) ? $nameArray[1] : "";
	$thisContact = array('firstName'=>$firstName, 'lastName'=>$lastName);
	array_push($this->contactsList, $thisContact);
    }

    private function searchContacts() {
	echo "Enter the search keyword" . "\n";
        $fp = fopen("php://stdin", "r");
	$keyword = ltrim(rtrim(fgets($fp, 100)));
        $keywordArray = $this->validateInputValue($keyword);
        $k_firstName = $keywordArray[0];
        $k_lastName = isset($keywordArray[1]) ? $keywordArray[1] : "" ;
	$matchedRecords = $this->search($k_firstName, $k_lastName);
	$this->printResults($matchedRecords);
    }

    private function printResults($matchedRecords) {
	$resultStringArr = array();
	foreach($matchedRecords as $record) {
	    array_push($resultStringArr, $record['firstName'] . ' ' . $record['lastName']);
	}
	usort($resultStringArr, function($a, $b) { return strlen($a) - strlen($b); });
	foreach($resultStringArr as $result) {
	    echo $result . "\n";
 	}
	echo "\n";
    }


    private function search($k_firstName, $k_lastName) {
	$matchedArray = array();
	foreach ($this->contactsList as $contact) {
	    if(stripos($contact['firstName'], $k_firstName) !== false || stripos($contact['firstName'], $k_lastName) !== false || stripos($contact['lastName'], $k_firstName) !== false || stripos($contact['lastName'], $k_lastName) !== false) {
		array_push($matchedArray, $contact);
	    }
	}
	return $matchedArray;
    }

    private function validateInputValue($name) {
	if(!ctype_alpha(str_replace(' ', '', $name))) {
            echo "That does not look like a generic input, only alphabets allowed!"; return false;
        }
        $nameArray = preg_split('/\s+/', $name); //explode(" ", $name);

        if(count($nameArray) > 2) {
            echo "We only support first and last name as of now!"; return false;
        }
	return $nameArray;
    }

    private function validateInput($input) {
	if($input < 1 || $input > 3) {
	    echo "Invalid input, check the menu below !" . "\n \n";
  	}
	else if($input == 3) {
            echo "Thanks, come again !!!" . "\n \n"; EXIT();
        }
        return true;
    }

    private function showMenu( &$input ) {
        echo "1) Add Contact, 2) Search, 3) Exit Application" . "\n";
        fscanf(STDIN, "%sn", $input);
    }

}

$contacts = new Contacts();
echo "\n\n\n" . "Welcome to your contacts manager, Please select one of the following" . "\n";
echo "Tip : Add contacts before searching" . "\n";
$contacts->run();
